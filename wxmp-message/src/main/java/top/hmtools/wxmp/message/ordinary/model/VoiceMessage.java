package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 语音消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1357290913</CreateTime>
	<MsgType><![CDATA[voice]]></MsgType>
	<MediaId><![CDATA[media_id]]></MediaId>
	<Format><![CDATA[Format]]></Format>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.voice)
public class VoiceMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7630075908133536334L;

	/**
	 * 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamAlias("MediaId")
	private String mediaId;
	
	/**
	 * 语音格式，如amr，speex等
	 */
	@XStreamAlias("Format")
	private String format;
	
	/**
	 * 语音识别结果，UTF8编码
	 */
	@XStreamAlias("Recognition")
	private String recognition;
	
	
	/**
	 * 语音识别结果，UTF8编码
	 * @return
	 */
	public String getRecognition() {
		return recognition;
	}

	/**
	 * 语音识别结果，UTF8编码
	 * @param recognition
	 */
	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "VoiceMessage [mediaId=" + mediaId + ", format=" + format + ", recognition=" + recognition
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
	
}
