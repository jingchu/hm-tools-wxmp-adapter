package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 回复视频消息
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>12345678</CreateTime>
  <MsgType><![CDATA[video]]></MsgType>
  <Video>
    <MediaId><![CDATA[media_id]]></MediaId>
    <Title><![CDATA[title]]></Title>
    <Description><![CDATA[description]]></Description>
  </Video>
</xml>
 * }
 * @author hybo
 *
 */
public class ReplyVideoMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7578998373624258879L;

	/**
	 * 回复视频消息 中 的视频信息
	 */
	@XStreamAlias("Video")
	private ReplyVideo video;

	public ReplyVideo getVideo() {
		return video;
	}

	public void setVideo(ReplyVideo video) {
		this.video = video;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "ReplyVideoMessage [video=" + video + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

}
