package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 语音/音频（注意此处media_id需通过素材管理->新增素材来得到）：
 * @author HyboWork
 *
 */
public class VoiceTagGroupSendParam extends BaseTagGroupSendParam {

	private MediaId voice;

	public MediaId getVoice() {
		return voice;
	}

	public void setVoice(MediaId voice) {
		this.voice = voice;
	}

	@Override
	public String toString() {
		return "VoiceTagGroupSendParam [voice=" + voice + ", filter=" + filter + ", msgtype=" + msgtype + "]";
	}

}
