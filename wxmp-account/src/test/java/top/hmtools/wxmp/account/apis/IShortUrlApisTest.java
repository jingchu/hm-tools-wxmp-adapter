package top.hmtools.wxmp.account.apis;

import org.junit.Test;

import top.hmtools.wxmp.account.BaseTest;
import top.hmtools.wxmp.account.models.ShorturlParam;
import top.hmtools.wxmp.account.models.ShorturlResult;

public class IShortUrlApisTest extends BaseTest{
	
	private IShortUrlApis shortUrlApis ;
	

	@Test
	public void testGetShorUrl() {
		ShorturlParam shorturlParam = new ShorturlParam();
		shorturlParam.setAction("long2short");
		shorturlParam.setLong_url("https://3w.huanqiu.com/a/3458fa/7PhIUEtAEzS?agt=8");
		ShorturlResult shorUrl = this.shortUrlApis.getShorUrl(shorturlParam);
		this.printFormatedJson("长链接转短链接接口", shorUrl);
	}


	@Override
	public void initSub() {
		this.shortUrlApis = this.wxmpSession.getMapper(IShortUrlApis.class);
	}

}
