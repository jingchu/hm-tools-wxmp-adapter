package top.hmtools.wxmp.account.models;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * 长URL转短URL用返回结果实体类
 * @author Hybomyth
 *
 */
public class ShorturlResult extends ErrcodeBean {

	/**
	 * 短链接。
	 */
	private String short_url;

	public String getShort_url() {
		return short_url;
	}

	public void setShort_url(String short_url) {
		this.short_url = short_url;
	}

	@Override
	public String toString() {
		return "ShorturlResult [short_url=" + short_url + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
