package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class SendPicItem {
	
	/**
	 * 图片的MD5值，开发者若需要，可用于验证接收到图片
	 */
	@XStreamAlias("PicMd5Sum")
	private String picMd5Sum;

	/**
	 * 图片的MD5值，开发者若需要，可用于验证接收到图片
	 * @return
	 */
	public String getPicMd5Sum() {
		return picMd5Sum;
	}

	/**
	 * 图片的MD5值，开发者若需要，可用于验证接收到图片
	 * @param picMd5Sum
	 */
	public void setPicMd5Sum(String picMd5Sum) {
		this.picMd5Sum = picMd5Sum;
	}

	@Override
	public String toString() {
		return "SendPicItem [PicMd5Sum=" + picMd5Sum + "]";
	}
	
	
}
