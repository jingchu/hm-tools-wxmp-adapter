package top.hmtools.wxmp.user.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class TagsWapperResult extends ErrcodeBean {

	private List<TagResult> tags;

	public List<TagResult> getTags() {
		return tags;
	}

	public void setTags(List<TagResult> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "TagsWapperResult [tags=" + tags + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
